package com.trial.lms.constants.error;

public class AuthorNotFoundError extends RuntimeException {

    public AuthorNotFoundError(String message) {
        super(message);
    }

    public AuthorNotFoundError(String message, Throwable cause) {
        super(message, cause);
    }
}
