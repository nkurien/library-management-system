package com.trial.lms.service;

import com.trial.lms.constants.error.AuthorNotFoundError;
import com.trial.lms.entities.AuthorEntity;
import com.trial.lms.entities.BooksEntity;
import com.trial.lms.repositories.AuthorRepository;
import com.trial.lms.repositories.BooksRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BooksService {

    private final BooksRepository booksRepository;
    private final AuthorRepository authorRepository;

    public List<BooksEntity> getAllBooks() {
        return booksRepository.findAll();
    }

    public BooksEntity createBook(BooksEntity book) {
        Optional<AuthorEntity> authorEntity = authorRepository.findById(book.getAuthorId());
        if (authorEntity.isPresent()) {
            booksRepository.save(book);
            return book;
        }
        throw new AuthorNotFoundError("Author with id :" + book.getAuthorId() + " not found");
    }
}
