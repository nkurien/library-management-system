package com.trial.lms.service;

import com.trial.lms.entities.AuthorEntity;
import com.trial.lms.repositories.AuthorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AuthorService {
    private final AuthorRepository repository;

    public List<AuthorEntity> getAllAuthors() {
        return repository.findAll();
    }

    public AuthorEntity createAuthor(AuthorEntity author) {
        repository.save(author);
        return author;
    }
}
