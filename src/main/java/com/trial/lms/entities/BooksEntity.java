package com.trial.lms.entities;

import com.trial.lms.config.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "books")
public class BooksEntity extends BaseEntity {

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "isbn", nullable = false, unique = true)
    private String isbn;

    @Column(name = "author_id", nullable = false)
    private long authorId;

    @Column(name = "year_of_release", nullable = false)
    private int yearOfRelease;
}
