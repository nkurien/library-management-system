package com.trial.lms.controller;

import com.trial.lms.entities.AuthorEntity;
import com.trial.lms.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/authors")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AuthorController {
    private final AuthorService service;

    @GetMapping(value = "", produces= MediaType.APPLICATION_JSON_VALUE)
    public List<AuthorEntity> getAllAuthors(){
        return service.getAllAuthors();
    }

    @PostMapping(value="", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public AuthorEntity createAuthor(@RequestBody AuthorEntity author) {
        return service.createAuthor(author);
    }
}
