package com.trial.lms.controller;

import com.trial.lms.entities.BooksEntity;
import com.trial.lms.service.BooksService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1/books")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BooksController {

    private final BooksService service;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BooksEntity> getAllBooks() {
        return service.getAllBooks();
    }

    @PostMapping(value="", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public BooksEntity createBook(@RequestBody BooksEntity book){
        return service.createBook(book);
    }
}
